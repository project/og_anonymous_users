<?php 
/**
 * @file
 * Settings form for Organic groups anonymous users module.
 *
 * @ingroup og_anonymous_users
 */
 
/**
 * Menu callback to load settings form.
 */ 
function og_anonymous_users_settings() {
  // Sanity check to ensure a group type is set.
  $is_configured = count(og_get_types('group'));
  if (!$is_configured) {
    form_set_error('content_types_table', t('You must designate at least one content type to act as a group node. <a href="!create">Create new content type</a> if needed.', array('!create' => url('admin/content/types/add', array('query' => drupal_get_destination())))));
  }
  $form['og_anonymous_users_settings']['global_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site-wide settings'),
    '#collapsible' => FALSE,
  );
  $form['og_anonymous_users_settings']['global_settings']['og_anonymous_users_global_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Globally Enable anonymous users to be added to OG group broadcasts/notifications.'),
    '#default_value' => variable_get('og_anonymous_users_global_enable', FALSE),
    '#description' => t('Unless this setting is enabled, notifications will NOT be sent out to any anonymous users who have previously signed up.'),
  );
  $form['og_anonymous_users_settings']['global_settings']['og_anonymous_users_user_register'] = array(
    '#type' => 'radios',
    '#title' => t('Default behavior when anonymous subscriber becomes a user'),
    '#options' => array(0 => t('Do nothing'), 1 => t('Remove anonymous subscription and give user account full group membership.')),
    '#default_value' => variable_get('og_anonymous_users_user_register', 1),
    '#description' => t('Specified behavior will occur if a user registers with an anonymously subscribed email address or an existing user changes email address to one which was previously subscribed anonymously.'),
  );
  $form['og_anonymous_users_settings']['content_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content types'),
    '#description' => t('You may adjust settings specific to individual content types on the content type settings pages linked below. Setting a type to Enabled or Disabled forces every node of the group type to have that setting. On Moderated, anonymous email addresses may only be added by a group admin.'),
    '#collapsible' => FALSE,
  );
  $header = array(t('Type'), t('Anonymous Users'), t('Operations'));
  $rows = array();
  foreach (og_get_types('group') as $type) {
    $type_url_str = str_replace('_', '-', $type);
    switch (variable_get('og_anonymous_users_enabled_'. $type, 0)) {
      case 0:
        $status = "Disabled";
        break;
      case 1:
        $status = "Enabled";
        break;
      case 2:
        $status = "Moderated";
        break;
    }
    $rows[] = array(
      $type,
      $status,
      l(t('Edit'), "admin/content/node-type/$type_url_str", array('query' => drupal_get_destination())),
    );
  }
  $form['og_anonymous_users_settings']['content_types']['content_types_table'] = array('#value' => theme('table', $header, $rows));
  return system_settings_form($form);
}
