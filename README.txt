
-- REQUIREMENTS --
* Organic Groups module - http://drupal.org/project/og 

-- INSTALLATION -- 
* Install as usual, see http://drupal.org/node/70151 for further information. 

-- Configuration --
* Go to Administer > Organic groups > Organic groups anonymous users settings
  - Enable the 'Global Enable' checkbox.
  - Verify the desired setting for default behavior for anonymous subscribers that create a user account.
  - Click 'Save'.
  - Click 'Edit' next to the organic group type you wish to allow anonymous subscribers for.
  - Under the 'Organic groups' fieldset, select the desired setting for that group type from the following:
    - Disabled
    - Enabled (users may be added by an admin or may subscribe themselves from the subscribe block)
    - Moderated (users may only be added by an admin of the group)
  - Click 'Save content type'.
* Go to Administer > Site Building > Blocks to enable the subscribe/unsubscribe block if 'Enabled' is chosen.

-- Use --
* From the list group members page (og/$nid/list), click the 'Anonymous subscribers' tab.
* Manage anonymous subscribers through this list.

-- 
-- Current maintainer --
* James Gross (james_g) - http://drupal.org/user/341166
